# Prepare data
1. Download Radiologist Annotations/Segmentations (XML format) from [here](https://wiki.cancerimagingarchive.net/display/Public/LIDC-IDRI#1966254194132fe653e4a7db00715f6f775c012) into folder LIDC-IDRI/
1. Download Images from same web source somewhere and [configure](https://pylidc.github.io/install.html) the pylidc config file.

# Run scripts
1. Convert data into more readable format NRRD
  ```python3 convert-data.py```
1. Compute metrics.
  ```python3 compute-metrics.py```

This will compute and store all the metrics and create a folder ```preprocessed-data``` where all the preprocessed data will be stored. Only contours with exactly four annotations (from four differen readers) are considered.


