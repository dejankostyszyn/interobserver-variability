import pylidc as pl
from pylidc.utils import consensus
import nrrd, os
import numpy as np

def write_nodules():
  """
  Copyright Dejan Kostyszyn 2021

  This method reads all XML files of the dataset
  https://wiki.cancerimagingarchive.net/display/Public/LIDC-IDRI#1966254194132fe653e4a7db00715f6f775c012
  """
  results_folder =  os.path.join("preprocessed-data")


  final_results = {}
  # Query for all CT scans with desired traits.
  pid = 'LIDC-IDRI-0001' # [LIDC-IDRI-0001, LIDC-IDRI-1012]

  # Iterate over all patients [LIDC-IDRI-0001, LIDC-IDRI-1012]
  ids = range(1, 1013)

  for id in ids:
    str_id = str(id)
    if len(str_id) == 1:
      pid = 'LIDC-IDRI-000' + str_id
    elif len(str_id) == 2:
      pid = 'LIDC-IDRI-00' + str_id
    elif len(str_id) == 3:
      pid = 'LIDC-IDRI-0' + str_id
    elif len(str_id) == 4:
      pid = 'LIDC-IDRI-' + str_id
    else:
      raise NotImplementedError

    # Create results folder.
    new_folder = os.path.join(results_folder, pid)
    if not os.path.exists(new_folder):
      os.makedirs(new_folder)

    try:
      # Take first scan, because this dataset only has one scan.
      scan = pl.query(pl.Scan).filter(pl.Scan.patient_id == pid).first()
      # vol = scan.to_volume() # If you want the according imaging.

      # Cluster nodules. One nodule contains all annotations of all observer.
      # Cluster function automatically adapt boundig box so that all
      # segmentation masks are at their actual position and at the same
      # time it minimizes the data.
      nodules = scan.cluster_annotations()
      voxelspacing = (scan.pixel_spacing, scan.slice_thickness, scan.slice_spacing)

      for i, anns in enumerate(nodules):

        # Combine all annotations of one nodule in one file.
        if len(anns) == 4:
          cmask, cbbox, masks = consensus(anns, clevel=0)

          # Create folder.
          foldername = os.path.join(new_folder, "nodule_" + str(i))
          if not os.path.exists(foldername):
            os.makedirs(foldername)
        
          for m_idx, mask in enumerate(masks):
            # Store contours on disk.
            filename = os.path.join(foldername, "annotation_" + str(m_idx) + ".nrrd")

            header = {}
            header["dimension"] = mask.ndim
            header["sizes"] = list(mask.shape)
            header['encoding'] = 'gzip'
            header["spacings"] = voxelspacing

            nrrd.write(filename=filename, data=np.uint8(mask), header=header)
            print(filename, "written.\n{}% done.".format(round((id+1)/len(ids)*100, 2)), end="\r", flush=True)
    except:
      pass
if __name__ == "__main__":
  write_nodules()