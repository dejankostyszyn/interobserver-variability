import os, nrrd, csv, sys, json
sys.path.insert(1, "../")
from os.path import join
from metrics import Metrics
import numpy as np
import i_utils

def run_experiment():
  root_dir = join("preprocessed-data")
  patients = os.listdir(root_dir)

  metrics_list = ["DSC", "HD", "PBD", "VS"]

  m = Metrics(metrics_list)

  results = {}
  for metric in metrics_list:
    results.setdefault(metric, [])

  # Compute metrics per nodule.
  for p_idx, patient in enumerate(patients):
    nod_folders = os.listdir(join(root_dir, patient))
    
    for nod_folder in nod_folders:
      if nod_folder != []:
        anns = os.listdir(join(root_dir, patient, nod_folder))
        data = []
        for ann in anns:
          d, h = nrrd.read(join(root_dir, patient, nod_folder, ann))
          data.append(d)

        voxelspacing = (h["spacings"])

        for i in range(len(data)-1):
          for j in range(i+1, len(data)):
            r = m.compute_metrics(data[i], data[j], voxelspacing=voxelspacing)

            for key in r.keys():
              results[key].append(r[key])

        print("{}% done.".format(round(((p_idx + 1) / len(patients))*100, 2)), end="\r", flush=True)

  with open('results.json', 'w') as convert_file: 
    convert_file.write(json.dumps(results))
  
  i_utils.write_summarized_results_into_file(results=results, filename='results.csv')

if __name__ == "__main__":
  run_experiment()