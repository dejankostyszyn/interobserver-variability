This code was only tested on Ubuntu 18.04 LTS

# Install requirements
I recommend utilizing a virtual environment

```
python3 -m venv venv                    # Create cirtual environment
source venv/bin/activate                # Activate virtual environment
python3 -m pip install --upgrade pip    # Upgrade pip
pip install -r requirements.txt         # Install requirements
```
# Run experiments
Each experiment is described in another README in the corresponding folder.