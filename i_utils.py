import numpy as np
import csv

def write_summarized_results_into_file(results, filename):
  attributes = [""]
  t_min = ["min"]
  t_max = ["max"]
  t_mean = ["mean"]
  t_median = ["median"]
  t_std = ["std"]

  for key, val in results.items():
    attributes.append(key)
    t_min.append(round(np.min(val), 2))
    t_max.append(round(np.max(val), 2))
    t_mean.append(round(np.mean(val), 2))
    t_median.append(round(np.median(val), 2))
    t_std.append(round(np.std(val), 2))

  with open(filename, 'w') as f:
    writer = csv.writer(f, delimiter=",")
    writer.writerow(attributes)
    writer.writerow(t_min)
    writer.writerow(t_max)
    writer.writerow(t_mean)
    writer.writerow(t_median)
    writer.writerow(t_std)