import numpy as np
import os, sys, math
from os.path import join
import i_utils
sys.path.insert(1, "../")
from metrics import Metrics
import nrrd, json, argparse

class Experiment():
  def __init__(self, data_root="data", results_path="results", no_reduction=False):
    self.data_root=data_root
    self.results_path=results_path
    self.no_reduction = no_reduction

    if not os.path.exists(self.results_path):
      os.makedirs(self.results_path)

  def check_data_consistency(self):
    # Check data consistency.
    print("Checking data consistency...")
    success = True

    patients = os.listdir(self.data_root)
    for p_idx, p in enumerate(patients):
        files = os.listdir(join(self.data_root, p))

        data = []
        for f in files:
            d, h = nrrd.read(join(self.data_root, p, f))# Some checks.
            _unique = np.unique(d).shape[0]
            _min = d.min()
            _max = d.max()
            if not (_unique <= 2 and _min >= 0 and _max <= 1):
              print("Tensors should contain <= 2 unique values with min >= 0 and max <= 1, but {} has unique = {}, min = {}, and max = {}.".format(
                join(self.data_root, p, f), _unique, _min, _max
              ))
              success = False
            data.append(d)

        # Check shape consistency.
        shape = data[0].shape
        for idx in range(1, len(data)):
          if shape != data[idx].shape:
            success = False
            print("Shapes in {} don't match!".format(p), end="\n\n")
        
        for idx, d in enumerate(data):
          l_succ = True

          # Check array properties.
          _min, _max, _unique = d.min(), d.min(), np.unique(d).shape[0]
          if not _min >= 0:
            l_succ = False
          elif not _min <= 1:
            l_succ = False
          elif not _unique <= 2:
            l_succ = False
          if not l_succ:
            success = False
            print("{} has min = {}, max = {}, unique = {}. Should be min >= 0, max <= 1, unique <= 2.".format(join(self.data_root, p, files[idx]), _min, _max, _unique), end="\n\n")

        for i in range(len(files)-1):
            for j in range(i+1, len(files)):
              # Compare arrays.
              if np.array_equal(data[i], data[j]):
                success = False
                print("{} and {} are equal!".format(files[i], files[j]))

        print("{:.2f}% done.".format((p_idx + 1)/len(patients)*100), end="\r", flush=True)

    if success:
      print("\nSuccess!", end="\n\n")
    else:
      print("\nSome errors occured! Please check output above!", end="\n\n")

  def shrink_data(self, data):
    shape = data[0].shape
    for idx in range(1, len(data)):
      if shape != data[idx].shape:
        raise ValueError("Shape mismatch: in1 and in2 must have the same shape, but have {} and {}.".format(shape, data[idx].shape))

    maxX, maxY, maxZ = 0, 0, 0
    minX, minY, minZ = data[0].shape[0], data[0].shape[1], data[0].shape[2]
    out = []

    for d in data:
      nonzeros = np.nonzero(d)
      _maxX, _maxY, _maxZ = nonzeros[0].max(), nonzeros[1].max(), nonzeros[2].max()
      _minX, _minY, _minZ = nonzeros[0].min(), nonzeros[1].min(), nonzeros[2].min()

      if _maxX > maxX:
        maxX = _maxX
      if _maxY > maxY:
        maxY = _maxY
      if _maxZ > maxZ:
        maxZ = _maxZ

      if _minX < minX:
        minX = _minX
      if _minY < minY:
        minY = _minY
      if _minZ < minZ:
        minZ = _minZ

    for idx, d in enumerate(data):
      out.append(d[minX:maxX, minY:maxY, minZ:maxZ])
      
    print("Reduced arrays from shape {} to shape {}".format(data[0].shape, out[0].shape))

    return out

  def run_experiment(self):
      print("Started experiment...")
      data_root = join(self.data_root)
      patients = os.listdir(data_root)
      
      metrics_list = ["DSC", "HD", "PBD", "VS"]

      m = Metrics(metrics_list)
      
      global_results = {}

      for metric in metrics_list:
        global_results.setdefault(metric, [])

      # Compute metrics.
      for p_idx, p in enumerate(patients):
          results = {}
          for metric in metrics_list:
            results.setdefault(metric, [])

          files = os.listdir(join(data_root, p))

          # Load data.
          data = []
          for f in files:
              d, h = nrrd.read(join(data_root, p, f))
              data.append(d)

          if not self.no_reduction:
            data = self.shrink_data(data)

          voxelspacing = (h["space directions"][0,0], h["space directions"][1,1], h["space directions"][2,2])

          idx = 1
          for i in range(len(files)-1):
            for j in range(i+1, len(files)):
              r = m.compute_metrics(data[i], data[j], voxelspacing=voxelspacing)

              for key in r.keys():
                results[key].append(r[key])

              idx += 1

          print("{:.2f}% done.".format((p_idx + 1)/len(patients)*100), end="\r", flush=True)
          i_utils.write_summarized_results_into_file(results=results, filename=join(self.results_path, str(p) + '-results.csv'))

          for key in results.keys():
            global_results[key] += results[key]

      # Write all results into one file.
      with open(join(self.results_path, 'results.json'), 'w') as convert_file: 
          convert_file.write(json.dumps(results))

      # Write summarized results into file.
      i_utils.write_summarized_results_into_file(results=global_results, filename=join(self.results_path, 'global-results.csv'))
      
if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(description='Run interobserver experiments.')
    parser.add_argument('--data_root', type=str, help='Path to root folder of your data.')
    parser.add_argument('--results_path', type=str, help='Path to results.')
    parser.add_argument('--check', action='store_true')
    parser.add_argument('--no_reduction', action='store_true')
    args = parser.parse_args()

    experiment = Experiment(data_root=args.data_root, results_path=args.results_path, no_reduction=args.no_reduction)
    if args.check:
      experiment.check_data_consistency()
    experiment.run_experiment()
    print("Successfully finished!")